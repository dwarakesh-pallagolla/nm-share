package com.example.cameranotify;

import java.io.IOException;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

import android.os.Bundle;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.text.format.Formatter;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceInfo;
import javax.jmdns.ServiceListener;

public class MainActivity extends Activity {

	String ip[] = new String[10];
	SharedPreferences prefs;
	int count = 0;

	private String getIpAddress() {
		String ip = "";
		try {
			Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
					.getNetworkInterfaces();
			while (enumNetworkInterfaces.hasMoreElements()) {
				NetworkInterface networkInterface = enumNetworkInterfaces
						.nextElement();
				Enumeration<InetAddress> enumInetAddress = networkInterface
						.getInetAddresses();
				while (enumInetAddress.hasMoreElements()) {
					InetAddress inetAddress = enumInetAddress.nextElement();

					if (inetAddress.isSiteLocalAddress()) {
						ip += inetAddress.getHostAddress();
					}

				}

			}

		} catch (SocketException e) {
			e.printStackTrace();
			ip += "Something Wrong! " + e.toString() + "\n";
		}

		return ip;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// set
		setContentView(R.layout.activity_main);

//		ComponentName receiver = new ComponentName(getApplicationContext(),
//				CameraEventReciver.class);
//		PackageManager pm = getApplicationContext().getPackageManager();
//
//		pm.setComponentEnabledSetting(receiver,
//				PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
//				PackageManager.DONT_KILL_APP);
//
		prefs = getSharedPreferences("Mypref", Context.MODE_PRIVATE);
		int size = prefs.getInt("array_size", 0);
		for(int i=1; i<size+1; i++)
		{
			try{
				TextView t = (TextView) findViewById(R.id.ips);
				t.setText(prefs.getString("array_" + i, null) + "\n"+t.getText());
			}
			catch (Exception e){
				e.printStackTrace();
				Log.e("MAIN", "SEE THIS");
			}
		}
		

	}

	public void RegisterDevice(View v) {
		// Register on network for other devices to discover
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					// Your code goes here
					discoverDevices();
					serviceInfo = ServiceInfo.create(type, "AndroidTest", 0,
							"plain test service from android");
					jmdns.registerService(serviceInfo);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		thread.start();
		Log.i("REGISTER", "Registered Device");
	}

	public void StartL(View v) {
		ComponentName receiver = new ComponentName(getApplicationContext(),
				CameraEventReciver.class);
		PackageManager pm = getApplicationContext().getPackageManager();
		pm.setComponentEnabledSetting(receiver,
				PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
				PackageManager.DONT_KILL_APP);
	}

	public void StopL(View v) {
		ComponentName receiver = new ComponentName(getApplicationContext(),
				CameraEventReciver.class);
		PackageManager pm = getApplicationContext().getPackageManager();
		pm.setComponentEnabledSetting(receiver,
				PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
				PackageManager.DONT_KILL_APP);
	}

	public void StopService(View v) {
		// stop the background activity here and stop pinging
		Log.i("main", "clicked stop service");
		stopService(new Intent(getBaseContext(), myService.class));
	}

	public void StartService(View v) {
		Log.i("main", "clicked start service");
		startService(new Intent(getBaseContext(), myService.class));
	}

	public void GenerateCode(View v) {
		Intent intend = new Intent("com.google.zxing.client.android.ENCODE");
		intend.putExtra("ENCODE_TYPE", "TEXT_TYPE");
		intend.putExtra("ENCODE_DATA", getIpAddress());
		startActivity(intend);
	}

	public void ScanCode(View v) {
		IntentIntegrator integrator = new IntentIntegrator(MainActivity.this);
		integrator.initiateScan();
	}

	public void ClearData(View view) {
		Editor edit = prefs.edit();
		edit.clear();
		edit.commit();
		handler.postDelayed(new Runnable() {
			public void run() {
				TextView t = (TextView) findViewById(R.id.ips);
				t.setText("");
			}
		}, 1);
	}

	public void scanDevices(View view) {
		Thread thread = new Thread(new Runnable() {
			@Override
			public void run() {
				try {
					// Discover devices on the network
					discoverDevices();
					
					ClearData(null);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		thread.start();
	}

	// FOR JMDNS

	android.net.wifi.WifiManager.MulticastLock lock;
	android.os.Handler handler = new android.os.Handler();
	private String type = "_nmshare._tcp.local.";
	private JmDNS jmdns = null;
	private ServiceListener listener = null;
	private ServiceInfo serviceInfo;

	// END of JDNS
	private void discoverDevices() {
		android.net.wifi.WifiManager wifi = (android.net.wifi.WifiManager) getSystemService(android.content.Context.WIFI_SERVICE);
		@SuppressWarnings("deprecation")
		String ip = Formatter.formatIpAddress(wifi.getConnectionInfo()
				.getIpAddress());
		lock = wifi.createMulticastLock("mylockthereturn");
		lock.setReferenceCounted(true);
		lock.acquire();
		try {
			Log.i("DISCOVER", "Discovering devices");
			jmdns = JmDNS.create(InetAddress.getByName(ip));
			jmdns.addServiceListener(type, listener = new ServiceListener() {

				@Override
				public void serviceResolved(ServiceEvent ev) {
					String ipaddresses[] = ev.getInfo().getHostAddresses();
					String ipaddr = "\n";
					int i;
					Editor edit = prefs.edit();
					try {
						count = prefs.getInt("array_size", 0);
						count++;
					} catch (Exception e) {
						count = 0;
						Log.i("COUNT", "Size init to 0");
					}
					
					for (i = 0; i < ipaddresses.length; i++) {
						// TODO make sure not more than 10 get added
						if (!ipaddresses[i].equals(getIpAddress())) {
//							ipaddr = ipaddr + ipaddresses[i] + "\n";
//							notifyUser(String.valueOf(i + 1) + "."
//									+ ipaddresses[i]);
//							edit.putString("array_" + Integer.toString(i),
//									ipaddresses[i]);
//							edit.commit();

//							
							edit.putInt("array_size", i+1);
							Log.i("MAINARRAY", Integer.toString(i+1));
							edit.putString("array_" + Integer.toString(i+1), ipaddresses[i]);
							notifyUser(ipaddresses[i]);
						}
					}
					edit.putInt("array_size", i);
					edit.commit();
				}

				@Override
				public void serviceRemoved(ServiceEvent ev) {
//					notifyUser("Service removed: " + ev.getName());
				}

				@Override
				public void serviceAdded(ServiceEvent event) {
					// Required to force serviceResolved to be called again
					// (after the first search)
					jmdns.requestServiceInfo(event.getType(), event.getName(),
							1);
				}
			});
			// serviceInfo = ServiceInfo.create(type, "AndroidTest", 0,
			// "plain test service from android");
			// jmdns.registerService(serviceInfo);
		} catch (IOException e) {
			e.printStackTrace();
			return;
		}
	}

	private void notifyUser(final String msg) {
		handler.postDelayed(new Runnable() {
			public void run() {
				TextView t = (TextView) findViewById(R.id.ips);
				t.setText(msg + "\n" + t.getText());
			}
		}, 1);
	}

	@Override
	protected void onStop() {
		// stop the listner and unregister all services related to JMDNS
		if (jmdns != null) {
			if (listener != null) {
				jmdns.removeServiceListener(type, listener);
				listener = null;
			}
			jmdns.unregisterAllServices();
			try {
				jmdns.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			jmdns = null;
		}

		if (lock != null) {
			lock.release();
			lock = null;
		}
		// lock.release();
		super.onStop();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
	}

	public void onActivityResult(int requestCode, int resultCode, Intent data) {

		String contents = null;
		IntentResult scanResult = IntentIntegrator.parseActivityResult(
				requestCode, resultCode, data);
		// super.onActivityResult(requestCode, resultCode, data);
		Log.i("result", Integer.toString(requestCode));
		if (scanResult != null) {
			// contents = data.getStringExtra("SCAN_RESULT");
			// String format = data.getStringExtra("SCAN_RESULT_FORMAT");
			// moved here
			contents = scanResult.getContents();
			try {
				count = prefs.getInt("array_size", 0);
				count++;
			} catch (Exception e) {
				count = 0;
				Log.i("COUNT", "Size init to 0");
			}
			if (contents != null) {
				ip[count] = contents;
				Log.i("IP GOT", contents);
				Editor edit = prefs.edit();
				edit.putInt("array_size", count);
				Log.i("MAINARRAY", Integer.toString(count));
				edit.putString("array_" + Integer.toString(count), ip[count]);
				edit.commit();

				TextView ips = (TextView) findViewById(R.id.ips);
				ips.append(contents + "\n");
			} else {
				Toast.makeText(getApplicationContext(), "Didnt get a IP",
						Toast.LENGTH_LONG).show();
			}
			// Handle successful scan
		} else {
			Toast.makeText(getApplicationContext(), "Didnt get a ok result",
					Toast.LENGTH_LONG).show();
		}

	}
}

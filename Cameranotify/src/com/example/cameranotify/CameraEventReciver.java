package com.example.cameranotify;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.util.Log;
import android.widget.Toast;
import com.example.cameranotify.MainActivity;

public class CameraEventReciver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {

		SharedPreferences prefs = context.getSharedPreferences("Mypref",
				Context.MODE_PRIVATE);
		Cursor cursor = context.getContentResolver().query(intent.getData(),
				null, null, null, null);
		cursor.moveToFirst();
		String image_path = cursor.getString(cursor.getColumnIndex("_data"));
		String data = "New Photo is Saved as : -" + image_path;
		Toast.makeText(context, data, Toast.LENGTH_LONG).show();
		Log.i("BROADCAST", data);
		int size = prefs.getInt("array_size", 0);
		// int size = new MainActivity().getsize();
		String ip[] = new String[10];
		Log.i("ARRAY", Integer.toString(size));
		// String[] array = new String[size];
		for (int i = 1; i < size + 1; i++) {
			try {
				ip[i] = prefs.getString("array_" + i, null);
				Log.i("ARRAY", ip[i]);
				MyClientTask myClientTask = new MyClientTask(ip[i], 12345,
						image_path);
				myClientTask.execute();
			} catch (Exception e) {
				Toast.makeText(context, "couldnt send image", Toast.LENGTH_LONG)
						.show();
				// e.printStackTrace();
			}

		}

	}

}

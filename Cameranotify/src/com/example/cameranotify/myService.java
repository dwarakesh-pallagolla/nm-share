package com.example.cameranotify;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.webkit.WebView.FindListener;
import android.widget.TextView;
import android.widget.Toast;

public class myService extends Service

{

	int lim = 0;
	ServerSocket ss;
	Socket s;
	private Handler handler;

	public class serverThread extends Thread {

		@Override
		public void run() {

			if (lim == 1) {

				try {
					Log.i("service", "inside server try");
					Boolean end = false;
					int filesize = 6022386;
					long start = System.currentTimeMillis();
					int bytesRead;
					int current = 0;

					ss = new ServerSocket(12345);

					while (!end) {
						Log.i("service", "inside server while");
						// Server is waiting for client here, if needed
						s = ss.accept();
						// BufferedReader input = new BufferedReader(
						// new InputStreamReader(s.getInputStream()));
						// PrintWriter output = new
						// PrintWriter(s.getOutputStream(),
						// true); // Autoflush
						// String st = input.readLine();

						byte[] mybytearray = new byte[filesize];
						InputStream is = s.getInputStream();
						String loc = "/sdcard/Pictures/" + Long.toString(start)
								+ ".jpg";

						// / adding image location to local memory

						SharedPreferences sharedPreferences = PreferenceManager
								.getDefaultSharedPreferences(getApplicationContext());

						Editor editor = sharedPreferences.edit();

						editor.putString("ImageLocation", loc);

						editor.commit();
						Log.i("ImageLocation saved", "" + loc);

						// / adding image location to local memory

						FileOutputStream fos = new FileOutputStream(loc); // destination
																			// path
																			// and
																			// name
																			// of
																			// file
						BufferedOutputStream bos = new BufferedOutputStream(fos);
						bytesRead = is.read(mybytearray, 0, mybytearray.length);
						current = bytesRead;

						do {
							bytesRead = is.read(mybytearray, current,
									(mybytearray.length - current));
							if (bytesRead >= 0)
								current += bytesRead;
						} while (bytesRead > -1);

						bos.write(mybytearray, 0, current);
						bos.flush();
						long end_time = System.currentTimeMillis();
						Log.i("TIME", Long.toString(end_time - start));
						bos.close();

						// Toast.makeText(getBaseContext(),
						// "got some message "+st, Toast.LENGTH_LONG);
						Log.i("Server", "Got image from client");

						handler.post(new Runnable() {
							@SuppressLint("NewApi")
							public void run() {
								Toast.makeText(myService.this, "Got new Image",
										Toast.LENGTH_LONG).show();
								Intent intent = new Intent();
								// File file = new File(loc);
								intent.setAction(Intent.ACTION_VIEW);

								// / reading image location from local memory

								SharedPreferences sharedPreferences = PreferenceManager
										.getDefaultSharedPreferences(getApplicationContext());
								String lo = sharedPreferences.getString(
										"ImageLocation", "Error");
								Log.i("ImageLocation retreived", "" + lo);

								// / reading image location from local memory
								intent.setDataAndType(
										Uri.parse("file://" + lo), "image/*");
								PendingIntent pIntent = PendingIntent
										.getActivity(myService.this, 0, intent,
												0);

								// build notification
								// the addAction re-use the same intent to keep
								// the example short
								Notification n = new Notification.Builder(
										myService.this)
										.setSmallIcon(R.drawable.ic_launcher)
										.setContentTitle("New Image")
										.setContentText("Got new Image")
										.setContentIntent(pIntent)
										.setAutoCancel(true).build();

								NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

								notificationManager.notify(0, n);
							}
						});

						// Toast.makeText(getBaseContext(), "Got new Image",
						// Toast.LENGTH_LONG).show();
						// Log.i("previous print","Good bye and thanks for all the fish :)");
						s.close();
						if (lim == 0) {
							end = true;
						}
					}
					ss.close();

				} catch (UnknownHostException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			} else {
				Log.i("service", "one service already started");

			}

		}

	}

	@Override
	public void onCreate() {

		Log.i("service", "creating service");
		handler = new Handler();

	}

	@Override
	public IBinder onBind(Intent arg0) {
		Log.i("service", "service onBind");
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {

		// / mod
		if (lim == 0) {
			lim++;
			Thread t = new serverThread();
			t.start();
			Toast.makeText(this, "service starting", Toast.LENGTH_LONG).show();

		} else {
			Log.i("service", "server alread running");
			Toast.makeText(this, "service already running", Toast.LENGTH_LONG)
					.show();
		}
		// / mod

		return START_STICKY;
		// when you return start sticky the service will run until you
		// explicitly stop it
	}

	@Override
	public void onDestroy() {

		lim = 0;
		Log.i("service", "I guess it is stopping now");
		Toast.makeText(this, "service stopping", Toast.LENGTH_LONG).show();
		try {
			if (ss != null)
				ss.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			Toast.makeText(this, "service stop error", Toast.LENGTH_LONG)
					.show();
		}
		try {
			if (s != null)
				s.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
			Toast.makeText(this, "service stop error", Toast.LENGTH_LONG)
					.show();
		}
		super.onDestroy();

	}

}

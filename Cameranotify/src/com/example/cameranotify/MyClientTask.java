package com.example.cameranotify;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.net.Socket;
import java.net.UnknownHostException;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Menu;

public class MyClientTask extends AsyncTask<Void, Void, Void> {
	
	
	SharedPreferences prefs;
	
	String dstAddress,selectedImagePath;
	int dstPort;
	
	MyClientTask(String addr, int port, String ImgPath){
		Log.i("asyncTask","inside async constructor");
		dstAddress = addr;
		dstPort = port;
		selectedImagePath = ImgPath;
	}
	
	
	@Override
	protected Void doInBackground(Void... arg0) {
		
		try {
			Thread.sleep(2000);
			Log.i("asyncTask","inside async");
	        Socket s = new Socket(dstAddress,dstPort);
	     // sendfile
            File myFile = new File (selectedImagePath); 
            byte [] mybytearray  = new byte [(int)myFile.length()];
            FileInputStream fis = new FileInputStream(myFile);
            BufferedInputStream bis = new BufferedInputStream(fis);
            bis.read(mybytearray,0,mybytearray.length);
            OutputStream os = s.getOutputStream();
            os.write(mybytearray,0,mybytearray.length);
            os.flush();

	        s.close();
	        


	} catch (UnknownHostException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	} catch (IOException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	} catch (InterruptedException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		return null;
	}
	
	@Override
	protected void onPostExecute(Void result) {
//		Log.i("async","in post execute");
		super.onPostExecute(result);
	}

	
	
}

